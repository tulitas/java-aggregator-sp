package aggregator.controllers;

import aggregator.Policy.PolicyCalculator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class PremiumCalculatorController {

    @RequestMapping(value = "result" , method = RequestMethod.POST)
    public String askSum(String inputDate, Model model) {

        System.out.println(inputDate);
         PolicyCalculator policyCalculator = new PolicyCalculator();
         policyCalculator.run(inputDate);

        double premiumFlatFire = policyCalculator.getPremiumFire();
        model.addAttribute("premiumFlatFire", premiumFlatFire);

        return "insurer/result";
    }

}
